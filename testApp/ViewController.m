//
//  ViewController.m
//  testApp
//
//  Created by Yulia Shlykova on 5/22/19.
//  Copyright © 2019 Yulia Shlykova. All rights reserved.
//

#import "ViewController.h"
#import "DataLoader.h"

static NSString* const hostResponceFile = @"http://localhost:8000/high_scores.json";

@interface ViewController () <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *dataTable;

@property NSArray *data;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataTable.delegate = self;
    self.dataTable.dataSource = self;
    self.data = [[DataLoader alloc] loadFromJSON:hostResponceFile];
}

- (IBAction)loadData:(id)sender {
    self.data = [[DataLoader alloc] loadFromJSON:hostResponceFile];
    NSLog(@"json: %@", self.data);
    [self.dataTable reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.data count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *cellReuseIdentifier = @"SimpleDataCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellReuseIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellReuseIdentifier];
    }
    
    NSString *player = [[self.data objectAtIndex:indexPath.row] valueForKey:@"firstName"];
    NSString *score = [[self.data objectAtIndex:indexPath.row] valueForKey:@"score"];
    NSString *cellText = [NSString stringWithFormat:@"%@: %@", player, score];
    
    [cell.textLabel setText:cellText];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"Cell will display");
}


@end
