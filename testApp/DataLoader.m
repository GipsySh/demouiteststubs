//
//  DataLoader.m
//  testApp
//
//  Created by Yulia Shlykova on 6/4/19.
//  Copyright © 2019 Yulia Shlykova. All rights reserved.
//

#import "DataLoader.h"

@interface DataLoader ()

@property NSArray *data;

@end

@implementation DataLoader

- (instancetype)init
{
    self = [super init];
    if (self) {
        return self;
    }
    return self;
}

-(NSArray *)loadFromJSON:(NSString *)jsonPath
{
    NSError *error;
    NSString *url_string = [NSString stringWithFormat: @"%@", jsonPath];
    NSData *data = [NSData dataWithContentsOfURL: [NSURL URLWithString:url_string]];
    
    if (data) {
        NSMutableArray *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        NSLog(@"json: %@", json);
        
        NSArray *data = [[NSArray alloc] initWithArray:json];
        return data;
    }
    else {
        NSLog(@"json loading failed");
        return nil;
    }
    
    return nil;
}

@end
