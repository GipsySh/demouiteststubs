//
//  Player.m
//  testApp
//
//  Created by Yulia Shlykova on 6/4/19.
//  Copyright © 2019 Yulia Shlykova. All rights reserved.
//

#import "Player.h"

@implementation Player

- (instancetype)initWithName:(NSString *)name score:(NSInteger)score
{
    self = [super init];
    if (self) {
        self.name = name;
        self.score = score;
    }
    return self;
}

@end
