//
//  DataLoader.h
//  testApp
//
//  Created by Yulia Shlykova on 6/4/19.
//  Copyright © 2019 Yulia Shlykova. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface DataLoader : NSObject

-(NSArray *)loadFromJSON:(NSString *)jsonPath;

@end

NS_ASSUME_NONNULL_END
