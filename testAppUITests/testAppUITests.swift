//
//  testAppUITests.swift
//  testAppUITests
//
//  Created by Yulia Shlykova on 5/22/19.
//  Copyright © 2019 Yulia Shlykova. All rights reserved.
//

import XCTest
import SBTUITestTunnel

class testAppUITests: XCTestCase {
    
    let url = "http://192.168.10.66:8000"
    let stubFile = "high_scores_test.json"

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        
        app.launchTunnel {
            let requestMatch = SBTRequestMatch(url: self.url)
            let stubResponce = SBTStubResponse(fileNamed: self.stubFile)

            self.app.stubRequests(matching: requestMatch, response: stubResponce);
            self.app.monitorRequests(matching: requestMatch)
        }

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        app.stubRequestsRemoveAll()
    }

    func testExample() {
        
        XCUIApplication().buttons["Load Data"].tap()
        
    }

}
